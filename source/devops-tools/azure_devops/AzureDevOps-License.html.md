---
layout: markdown_page
title: "Azure DevOps License Overview"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Pipeline Costs
* Open source (public) projects get 10 free parallel jobs, unlimited time
* private projects - MS hosted - 1 free parallel job, 1800 mins/mnth - $40/parallel addition, unlimited time
* private projects - self-hosted - 1 free parallel job, unlimited time, each Visual Studio Enterprise subscriber in org = 1 additional self-hosted parallel job, beyond that +$15 each additional parallel job
    * ***NOTE*** Their pricing is pushing folks to their cloud service offering. On-prem Pipelines Server = buying TFS licenses (now called Azure DevOps Server). This version will be updated 3-4 months behind in feature/functionality updates. 
    * [Azure Pipelines Only Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-pipelines/)

## Artifacts Costs
* Free <=5 users
* additional $4/user above 5

## Repos Costs
* Free <=5 users
* Included in $30/month per 10 users

## Boards Costs
* Free <=5 users
* Included in $30/month per 10 users

## Test Plan Costs
* additional $52/month per user

## Azure DevOps Add-Ons

**Separate from Azure DevOps but available for extra cost**

* Azure Monitor - APM, infra, data, services **(GitLab has)**
    * App centric
    * but separate from Azure DevOps.
    * [https://docs.microsoft.com/en-us/azure/azure-monitor/overview](https://docs.microsoft.com/en-us/azure/azure-monitor/overview)
    * pay by use
* Visual Studio - Full blown IDE - Free with sub
* Visual Studio Code - IDE Lite - Free  **(GitLab has)**
* Container Registry (MS has Azure Container Registry) **(GitLab has)**

## Microsoft Pricing Links

**Azure DevOps**

* [Azure DevOps Services Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-devops-services/)
* [Azure DevOps On-prem](https://azure.microsoft.com/en-us/pricing/details/devops/on-premises/) = See TFS Pricing

**Visual Studio**

* Current VSTS and MSDN subscribers get different levels of Azure DevOps. Details can be found at [Azure DevOps for Visual Studio subscribers](https://docs.microsoft.com/en-us/visualstudio/subscriptions/vs-azure-devops)

* [VSTS Pricing](https://visualstudio.microsoft.com/team-services/pricing/)

Visual Studio ‘Professional Version’ is the most comparable to GitLab since Visual Studio ‘Enterprise Version’ includes extras outside the scope of DevOps (such as MS Office, etc).

Visual Studio Professional can be purchased under a ‘standard’ or ‘cloud’ model.

- Standard = $1,200 year one (retail pricing), then $800 annual renewals (retail pricing)
- Cloud - $540 per year

Under their ‘modern purchasing model’, the monthly cost for Visual Studio Professional (which includes TFS and CAL license) is $45 / mo ($540 / yr).  However, extensions to TFS such as [Test Manager](https://marketplace.visualstudio.com/items?itemName=ms.vss-testmanager-web) ($52/mo), [Package Management](https://marketplace.visualstudio.com/items?itemName=ms.feed) ($15/mo), and [Private Pipelines](https://marketplace.visualstudio.com/items?itemName=ms.build-release-private-pipelines) ($15/mo) require an additional purchase.

**Team Foundation Server**

[TFS Pricing](https://visualstudio.microsoft.com/team-services/tfs-pricing/)

A TFS license can be purchased as standalone product, but a TFS license (and CAL license) is also included when you buy a Visual Studio license / subscription.

MS pushes Visual Studio subscriptions and refers customers who are only interested in a standalone TFS with a ‘classic purchasing’ model to license from a reseller.

Excluding CapEx and Windows operating system license, a standalone TFS license through a reseller in classic purchasing model is approximately $225 per year per instance.  The approximate Client Access License is approximately $320 per year.

## General Notes
* All paid plans include unlimited stakeholder users who can view and contribute to work items and boards, and view dashboards, charts, and pipelines
* Release details and [roadmap](https://docs.microsoft.com/en-us/azure/devops/release-notes/)
* All current VSTS subscribers will be moved automatically to Azure DevOps.
* TFS (on prem) pricing implies a SaaS first mentality and customer push
    * Buy at least one Visual Studio license + Azure DevOps users @ $6/mnth
        * Visual Studio Professional ($45/mnth) - no Test Manager, Artifacts, Pipelines (unless OSS)
        * Visual Studio Enterprise ($250/mnth) - include Test Manager and Artifacts
    * [https://visualstudio.microsoft.com/team-services/tfs-pricing/](https://visualstudio.microsoft.com/team-services/tfs-pricing/)
