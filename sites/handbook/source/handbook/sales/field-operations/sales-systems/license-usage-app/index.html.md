---
layout: handbook-page-toc
title: "License Usage Salesforce App"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to use the License App

1. Navigate to any customer account in Salesforce.
1. Click on the License Usage button at the top of the layout.
![License Usage Button On Account Layout](/handbook/sales/field-operations/sales-systems/license-usage-app/buttonss.png)
1. Review the current usage of Activated vs Entitled users on all Subscriptions related to the Account.
![License Usage Button On Account Layout](/handbook/sales/field-operations/sales-systems/license-usage-app/appss.png)
1. To get a summary of license usage across all your accounts click "See License Usage on all my Accounts"
1. To give feedback or ideas for the License Usage app click "Give Feedback / Report Bug" and comment on the issue.

## Data Definitions

| Data Point              | Description                                  | Source              |
|-------------------------|----------------------------------------------|---------------------|
| Plan Name               | Subscription GitLab Tier                     | Zuora 360           |
| Hosting Type            | GitLab.com vs Self Managed                   | Data Team Dashboard |
| Licensed Users          | Quantity of licenses sold                    | Zuora 360           |
| Activated Users         | Quantity of users activated                  | Data Team Dashboard |
| Seat Price              | MAX of Seat Price                            | Zuora 360           |
| Overage Value           | If Over: (Activated - Licensed) x Seat Price | Calculated          |
| Subscription Start Date | Subscription Term Start                      | Zuora 360           |
| Subscription End Date   | Subscription Term End                        | Zuora 360           |

## Frequently Asked Questions (FAQ)

**Question: Activated Users is great, but when will I be able to see SMAU and other activity metrics?**<br />
Answer: The Product and Data teams are working to get these metrics at the subscription level, and when they do we will add to this dash! [Follow the progress here](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#heading=h.t3mpohrk83kp).


**Question: I viewed one of my Accounts, and some of the Subscription Data says "Not Available", what went wrong?**<br />
Answer: Activated User data is not yet available for some subscriptions based on Namespace collisons or requires enhancemen to Seatlink. Some subscriptions will never recieve their acticated user cound because of air gapped hosting. In either scenario please [post on the Feedback issue](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/912) if you think the customer's subscription data should be available.

**Question: I expected there to be more (or less) Subscriptions related to the Account then was is appearing, why is this?**<br />
Answer: Please confirm in the #Sales-Support slack room if you think viewing the app uncovered and issue with the customer's Subscriptions.