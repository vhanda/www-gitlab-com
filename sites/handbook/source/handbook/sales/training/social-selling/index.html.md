---
layout: handbook-page-toc
title: "Social Selling"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Social Selling Basics

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Ir7od3stk70" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## LinkedIn Sales Navigator
- [Sales Navigator Customer Hub: Resources](https://business.linkedin.com/sales-solutions/sales-navigator-customer-hub/resources)
- [Live and On-Demand Training Webinars](https://training.sales.linkedin.com/series/webinars)
- [Social Selling Index (SSI)](https://business.linkedin.com/sales-solutions/social-selling/the-social-selling-index-ssi)

## Social Selling Plays
- Coming soon
