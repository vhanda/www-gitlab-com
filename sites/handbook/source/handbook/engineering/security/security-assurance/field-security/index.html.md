---
layout: handbook-page-toc
title: "Field Security Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Field Security Mission
1. Serve as the public representation of GitLab's Security Department
1. Provide high levels of assurance to GitLab customers and prospects
1. Facilitate communication and collaboration between various GitLab teams and the Security Department
1. Support the sales process as it relates to security

<!-- Hold for future use ## Roadmap
Field Security does not yet have a roadmap but is planning to have one completed by the end of 2020--->


### Active Field Security work includes:
* Develop and Maintain the [Customer Assurance Package](/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html)
* Evanglize [Security Best Practices](/handbook/security/) both inside and outside of GitLab
* Sales support and training
* Ensure completion of [Customer Security Assessments](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html)
* Produce and maintain Self-Service Security Resources
* Maintain the security@gitlab.com security queue

### Control and Program/Project Owners
The following are the [directly responsible individuals](/handbook/people-group/directly-responsible-individuals/) (DRIs) for the different areas within the field security team:
* [Security@gitlab.com Security Queue](/handbook/engineering/security/security-assurance/field-security/security-zendesk.html) - @dsharris
* [SOC2 Type 1 Requests](/handbook/engineering/security/security-assurance/security-compliance/soc2.html#requesting-a-copy-of-the-gitlab-soc2-type-1-report) - @dsharris
* [Customer security assessments](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) - @dsharris
* [Customer assurance package](/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html) - @dsharris

### Makeup of the team
- @dsharris - https://about.gitlab.com/company/team/#dsharris
- @jburrows001 - Interim Manager - https://about.gitlab.com/company/team/#jburrows001
- @julia.lake - Director of Security Assurance - https://about.gitlab.com/company/team/#julia.lake
- **COMING SOON** - Manager of Field Security

## Contact the Field Security Team
* Email
   * `fieldsecurity@gitlab.com`
* Slack
   * Feel free to tag is with `@field-security`
   * The `#sec-fieldsecurity`, `#sec-assurance`, `#security-department` slack channels are the best place for questions relating to our team (please add the above tag)
* [GitLab field security project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/field-security)
